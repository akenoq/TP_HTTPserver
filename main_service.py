import os

import config
# from debugger import debug_print
from req_parser import ReqParser
from resp_builder import RespBuilder
import resp_src


class MainService:
    def __init__(self, data, root_dir):
        # print("main_service")
        self.root_dir = root_dir
        self.data = data.decode("utf-8")
        self.req = ReqParser(self.data)
        self.resp = RespBuilder()

    def get_response(self):
        if self.req.method in resp_src.ALLOW_METHOD:
            self.handle()
        else:
            self.resp.code = resp_src.STATUS['NOT_ALLOWED']
        return self.resp.build()

    def get_cont_type(self):
        file_type = self.req.path.split('.')[-1]
        cont_type = resp_src.TYPES[file_type] if file_type in resp_src.TYPES.keys() else ''
        return cont_type

    def handle(self):
        # debug_print("\n_HANDLER_WORKS_")
        host_path = os.path.normpath(self.root_dir + self.req.path)
        # debug_print("ROOT DIR = {}".format(self.root_dir))
        # debug_print("HOST PATH = {}\n".format(host_path))

        if os.path.commonprefix([host_path, self.root_dir]) != self.root_dir:
            return

        host_path_index = os.path.join(host_path, config.INDEX_PAGE)
        if os.path.isfile(host_path_index):
            host_path = host_path_index
        elif os.path.exists(host_path):
            self.resp.code = resp_src.STATUS['FORBIDDEN']
        try:
            f = open(host_path, 'rb')
            content = f.read()
            f.close()
            self.resp.content_length = len(content)
            if self.req.method == "HEAD":
                content = b''
            self.resp.body = content
            self.resp.content_type = self.get_cont_type()
            self.resp.code = resp_src.STATUS['OK']
        except IOError as e:
            # pass
            print("Error with " + e.filename)
