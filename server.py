import os
import socket

# from debugger import debug_print
from main_service import MainService


class MasterServer:
    workers = []

    def __init__(self, name, host, port, cpu, root_dir, buf_size, listeners):
        self.name = name
        self.address = (host, port)
        self.cpu_num = cpu  # workers
        self.root_dir = root_dir
        self.buf_size = buf_size
        self.listeners = listeners
        self.server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        self.service = MainService
        # self.k = [0, 0, 0, 0]
        self.parent_pid = 0

    def child_proc(self):
        print("Created worker on PID={}".format(os.getpid()))
        # index = os.getpid()-self.parent_pid

        while 1:
            (conn, addr) = self.server_socket.accept()
            data = conn.recv(self.buf_size)
            # self.k[index - 1] = self.k[index - 1] + 1
            if len(data.strip()) == 0:
                # print("No data")
                conn.close()
                continue

            # MainService в self
            service = self.service(data, self.root_dir)
            resp = service.get_response()
            # print(">>> PID ===>", os.getpid(), "===>", k)
            conn.sendall(resp)
            conn.close()
            # print("www = ", index, "vvv = ", self.k[index-1])

    def start(self):
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind(self.address)
        self.server_socket.listen(self.listeners)

        print("Server started on {}:{}".format(self.address[0], self.address[1]))
        print("PID={}".format(os.getpid()))

        self.parent_pid = os.getpid()

        for i in range(self.cpu_num):
            new_pid = os.fork()
            if new_pid == 0:
                self.child_proc()
                break
            else:
                self.workers.append(new_pid)

        # print(self.k)

        self.server_socket.close()

        for child_pid in self.workers:
            os.waitpid(child_pid, 0)
