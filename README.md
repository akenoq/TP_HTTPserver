# HTTP_server

Arch: Prefork

Lang: Python

Task: https://github.com/init/http-test-suite

### Running:
```
sudo docker build -t serv_test .
sudo docker run -p 80:8080 -ti 7450ddd997f4
```

## Result
### 2 CPU
```
$ ab -n 100000 -c 100 http://127.0.0.1:5933/httptest/wikipedia_russia.html
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 10000 requests
Completed 20000 requests
Completed 30000 requests
Completed 40000 requests
Completed 50000 requests
Completed 60000 requests
Completed 70000 requests
Completed 80000 requests
Completed 90000 requests
Completed 100000 requests
Finished 100000 requests


Server Software:        akenoq_server
Server Hostname:        127.0.0.1
Server Port:            5933

Document Path:          /httptest/wikipedia_russia.html
Document Length:        954824 bytes

Concurrency Level:      100
Time taken for tests:   78.593 seconds
Complete requests:      100000
Failed requests:        0
Total transferred:      95497100000 bytes
HTML transferred:       95482400000 bytes
Requests per second:    1272.37 [#/sec] (mean)
Time per request:       78.593 [ms] (mean)
Time per request:       0.786 [ms] (mean, across all concurrent requests)
Transfer rate:          1186600.09 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0       9
Processing:     3   78   8.6     76     206
Waiting:        1   76  10.0     76     205
Total:          5   78   8.6     77     206

Percentage of the requests served within a certain time (ms)
  50%     77
  66%     78
  75%     79
  80%     80
  90%     84
  95%     91
  98%    102
  99%    116
 100%    206 (longest request)
```
### 1 CPU
```
$ ab -n 100000 -c 100 http://127.0.0.1:5933/httptest/wikipedia_russia.html
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 10000 requests
Completed 20000 requests
Completed 30000 requests
Completed 40000 requests
Completed 50000 requests
Completed 60000 requests
Completed 70000 requests
Completed 80000 requests
Completed 90000 requests
Completed 100000 requests
Finished 100000 requests


Server Software:        akenoq_server
Server Hostname:        127.0.0.1
Server Port:            5933

Document Path:          /httptest/wikipedia_russia.html
Document Length:        954824 bytes

Concurrency Level:      100
Time taken for tests:   103.199 seconds
Complete requests:      100000
Failed requests:        0
Total transferred:      95497100000 bytes
HTML transferred:       95482400000 bytes
Requests per second:    969.00 [#/sec] (mean)
Time per request:       103.199 [ms] (mean)
Time per request:       1.032 [ms] (mean, across all concurrent requests)
Transfer rate:          903678.74 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0      21
Processing:     1  103  20.0     98     267
Waiting:        1  103  19.7     98     267
Total:          6  103  20.0     98     267

Percentage of the requests served within a certain time (ms)
  50%     98
  66%    100
  75%    102
  80%    104
  90%    121
  95%    152
  98%    173
  99%    189
 100%    267 (longest request)
```
### Nginx
```
akenoq@AkenoqPC:~/PYTHON_3/HTTPserver_test$ sudo service nginx start
akenoq@AkenoqPC:~/PYTHON_3/HTTPserver_test$ ab -n 100000 -c 100 127.0.0.1:80/httptest/wikipedia_russia.html
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient)
Completed 10000 requests
Completed 20000 requests
Completed 30000 requests
Completed 40000 requests
Completed 50000 requests
Completed 60000 requests
Completed 70000 requests
Completed 80000 requests
Completed 90000 requests
Completed 100000 requests
Finished 100000 requests


Server Software:        nginx/1.10.3
Server Hostname:        127.0.0.1
Server Port:            80

Document Path:          /httptest/wikipedia_russia.html
Document Length:        954824 bytes

Concurrency Level:      100
Time taken for tests:   27.766 seconds
Complete requests:      100000
Failed requests:        0
Total transferred:      95507100000 bytes
HTML transferred:       95482400000 bytes
Requests per second:    3601.56 [#/sec] (mean)
Time per request:       27.766 [ms] (mean)
Time per request:       0.278 [ms] (mean, across all concurrent requests)
Transfer rate:          3359123.93 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0       3
Processing:     8   27   3.8     27     126
Waiting:        0    1   3.3      0     124
Total:          9   28   3.8     27     128

Percentage of the requests served within a certain time (ms)
  50%     27
  66%     29
  75%     30
  80%     30
  90%     31
  95%     33
  98%     34
  99%     35
 100%    128 (longest request)
```