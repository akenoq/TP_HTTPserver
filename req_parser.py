from urllib.parse import unquote


class ReqParser:
    def __init__(self, data):
        self.method, self.path = self.get_method_path(data)

    @staticmethod
    def get_method_path(data):
        method, url = data.split(' ')[0:2]
        path = unquote(url.split('?')[0])
        return method, path
